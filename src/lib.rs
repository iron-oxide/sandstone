use std::thread;
use std::sync::mpsc;
use std::sync::{Arc, Mutex};

type JobQueue = Arc<Mutex<mpsc::Receiver<Message>>>;
type Job = Box<FnBox + Send + 'static>;

enum Message {
    NewJob(Job),
    Terminate,
}

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

impl ThreadPool {

    /// Create a new ThreadPool.
    ///
    /// The `size` is the number of threads in the pool.
    ///
    /// # Panics
    ///
    /// The `new` function will panic if the size is zero.
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        let (sender, receiver) = mpsc::channel();
        let receiver = Arc::new(Mutex::new(receiver));
        let mut workers = Vec::with_capacity(size);

        for id in 0..size {
            workers.push(Worker::new(id, receiver.clone()));
        }

        ThreadPool {
            workers,
            sender,
        }
    }

    /// Execute a closure one time.
    ///
    /// # Panics
    ///
    /// The `unwrap` on `send` will panic if the receiving end
    /// has stopped receiving messages. This should be impossible
    /// though because it isn't possible for the recieving end to
    /// stop as long as ThreadPool exists.
    pub fn execute<F>(&self, f: F)
        where
            F: FnOnce() + Send + 'static
    {
        let job = Box::new(f);

        self.sender.send(Message::NewJob(job)).unwrap();
    }

    pub fn join(&mut self) {
        for _ in &mut self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }
        while let Some(mut worker) = self.workers.pop() {
            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

/// Drop for ThreadPool.
///
/// Prevents ThreadPool from closing send or receive channels
/// before the threads have exited.
///
/// # Panics
///
/// If the call to `join` fails unwrap will panic and go
/// directly into an ungraceful shutdown.
impl Drop for ThreadPool {
    fn drop(&mut self) {
        for _ in &mut self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }
        for worker in &mut self.workers {
            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

trait FnBox {
    fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>) {
        (*self)()
    }
}

struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {

    /// Create a new Worker.
    ///
    /// The `id` is an identifier and the `receiver` is
    /// a queue of jobs that need to be executed. The
    /// call to `recv` blocks until a job becomes available.
    ///
    /// # Panics
    ///
    /// The `new` function will panic if it fails to aquire
    /// a `lock` on the mutex. This will happen if the mutex
    /// is _poisoned_ because another thread paniced while
    /// holding the lock instead of releasing it.
    ///
    /// It will also panic if the sending side of the `recv`
    /// call has shut down. This should not be possible however
    /// since the sending side will exist as long as the
    /// `ThreadPool`.
    fn new(id: usize, receiver: JobQueue) -> Worker {
        let thread = thread::spawn(move || {
            loop {
                let message = receiver
                    .lock()
                    .expect("Failed to aquire lock. Was the `Mutex` poisoned?")
                    .recv()
                    .unwrap();

                match message {
                    Message::NewJob(job) => job.call_box(),
                    Message::Terminate => break,
                }
            }
        });

        Worker {
            id,
            thread: Some(thread),
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::{Arc, Mutex};

    #[test]
    fn it_works() {
        let mut pool = ThreadPool::new(1);
        for _ in 0..5 {
            println!("It works!");
        }
        pool.join();
    }

    #[test]
    #[should_panic]
    fn fail_on_empty_pool() {
        let mut pool = ThreadPool::new(0);
        pool.join();
    }

    #[test]
    fn simple_sum() {
        let mut pool = ThreadPool::new(1);
        let acc = Arc::new(Mutex::new(0));
        for i in 0..6 {
            let acc = acc.clone();
            pool.execute(move || {
                let mut acc = acc.lock().unwrap();
                *acc += i;
            });
        }
        pool.join();
        assert_eq!(15, *acc.lock().unwrap());
    }
}
